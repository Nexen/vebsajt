$(function () {
	//$.stellar({ horizontalScrolling: false });
	$('[rel="tooltip"]').tooltip();
	$('.emailVerification').val("");

	$('.alert').delay(5000).slideUp("slow");
	$('.profileAnchor').click(function () {
		$('.showLink').show("slow");
	});
	$('.charAnchor').click(function () {
		$('.showCharLink').show("slow");
	});

	$('.subs').filter(function () {
		return $.trim($(this).text()) === ''
	}).hide()

	CKEDITOR.replace('ckEditor');

});
