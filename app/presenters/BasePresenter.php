<?php

namespace App\Presenters;

use Nette;
use Brabijan;
use App;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	use \Kdyby\Autowired\AutowireProperties;
	use \Kdyby\Autowired\AutowireComponentFactories;


	/** @var  App\Auth\AvatarsRepository @autowire */
	public $AvatarsRepository;

	/** @var  App\Characters\CharactersRepository @autowire */
	public $charactersRepository;

	/** @var  App\Auth\ChangelogRepository @autowire */
	public $changelogRepository;


	/** @var  App\Auth\PagesRepository @autowire */
	public $pagesRepository;

	/** @var  App\Auth\UptimeRepository @autowire */
	public $UptimeRepository;



	public function beforeRender()
	{
		$this->template->charactersOnlineAlliance = $this->charactersRepository->getOnlineCharacters("Alliance");
		$this->template->charactersOnlineHorde = $this->charactersRepository->getOnlineCharacters("Horde");
		$this->template->charactersOnlineNeutral = $this->charactersRepository->getOnlineCharacters("Neutral");

		$this->template->profilePicture = $this->AvatarsRepository->getPictureByUser($this->user->id);
		$this->template->changelog = $this->changelogRepository->getChangelog()->order("id DESC")->limit(8);
		$this->template->pages = $this->pagesRepository->getPages();
		$this->template->serverStatus = $this->getServerStatus();
		$this->template->serverUptime = $this->UptimeRepository->getUptime();

		if (PHP_OS == "Win") {
			$this->template->memoryUsage = $this->MemoryUsage();
		} else {
			$this->template->memoryUsage = $this->MemoryUsageLinuxOsx();
		}
	}



	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;
		$lostPassword = Nette\Utils\Html::el("a", "link text")->href("somewhere")->class("positionMe");

		$form->addText('email', 'Email: ')
			 ->setRequired('Please enter your email.')
			 ->setAttribute("class", "form-control");

		$form->addPassword('password', 'Heslo: ')
			 ->setRequired('Please enter your password.')
			 ->setAttribute("class", "form-control");

		$form->addCheckbox('remember', 'Zapamatovat si přihlášení');
		$link = \Nette\Utils\Html::el("a", array("href" => $this->link("LostPassword:default")))->setHtml("Zapomenuté heslo?");
		$form->addCheckbox("lostPassword", $link)
			 ->setAttribute("style", "display:none");

		$form->addSubmit('send', 'Přihlásit')
			 ->setAttribute("class", "btn btn-primary");

		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->signInFormSucceeded;

		return $form;
	}



	public function signInFormSucceeded($form)
	{
		$values = $form->getValues();

		if ($values->remember) {
			$this->getUser()->setExpiration('14 days', FALSE);
		} else {
			$this->getUser()->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->getUser()->login($values->email, $values->password);
			$this->flashMessage("Byl(a) si přihlášen(a)!", "success");
			$this->redirect('this');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
			$this->flashMessage("Špatné jméno nebo heslo, zapomněli jste heslo?", "danger");
		}
	}



	protected function createTemplate($class = NULL)
	{
		$template = parent::createTemplate($class);
		$template->registerHelper('getDatadisk', function ($s) {
			switch ($s) {
				default:
					$datadisk = "Unknown";
					break;
				case 0:
					$datadisk = "The Classic";
					break;
				case 1:
					$datadisk = "The Burning Crusade";
					break;
				case 2:
					$datadisk = "The Wrath of the Lich King";
					break;
				case 3:
					$datadisk = "The Cataclysm";
					break;
				case 4:
					$datadisk = "The Mists of Pandaria";
					break;
			}

			return $datadisk;
		});
		$template->registerHelper('getAuthorPicture', function ($s) {
			$avatar = $this->AvatarsRepository->getPictureByUser($s);
			if ($avatar) {
				return $avatar->avatar;
			} else {
				return $avatar = NULL;
			}
		});
		$template->registerHelper('getTalentSpec', function ($s, $class) {
			if ($class == 1) {
				//warrior
				switch ($s) {
					default:
						$spec = "";
						break;
					case "71 0 ":
						$spec = "Arms";
						break;
					case "72 0 ":
						$spec = "Fury";
						break;
					case "73 0 ":
						$spec = "Protection";
						break;
				}
			}
			if ($class == 2) {
				//paladin
				switch ($s) {
					default:
						$spec = "";
						break;
					case "65 0 ":
						$spec = "Holy";
						break;
					case "66 0 ":
						$spec = "Protection";
						break;
					case "70 0 ":
						$spec = "Retribution";
						break;
				}
			}
			if ($class == 3) {
				//Hunter
				switch ($s) {
					default:
						$spec = "";
						break;
					case "253 0 ":
						$spec = "Beast Mastery";
						break;
					case "254 0 ":
						$spec = "Marksmanship";
						break;
					case "255 0 ":
						$spec = "Survival";
						break;
				}
			}
			if ($class == 4) {
				//rogue
				switch ($s) {
					default:
						$spec = "";
						break;
					case "259 0 ":
						$spec = "Assassination";
						break;
					case "260 0 ":
						$spec = "Combat";
						break;
					case "261 0 ":
						$spec = "Subtlety";
						break;
				}
			}
			if ($class == 5) {
				//priest
				switch ($s) {
					default:
						$spec = "";
						break;
					case "256 0 ":
						$spec = "Discipline";
						break;
					case "257 0 ":
						$spec = "Holy";
						break;
					case "258 0 ":
						$spec = "Shadow";
						break;
				}
			}
			if ($class == 6) {
				//Death Knight
				switch ($s) {
					default:
						$spec = "";
						break;
					case "250 0 ":
						$spec = "Blood";
						break;
					case "251 0 ":
						$spec = "Frost";
						break;
					case "252 0 ":
						$spec = "Unholy";
						break;
				}
			}
			if ($class == 7) {
				//Shaman
				switch ($s) {
					default:
						$spec = "";
						break;
					case "262 0 ":
						$spec = "Elemental";
						break;
					case "263 0 ":
						$spec = "Enhancement";
						break;
					case "264 0 ":
						$spec = "Restoration";
						break;
				}
			}
			if ($class == 8) {
				//mage
				switch ($s) {
					default:
						$spec = "";
						break;
					case "63 0 ":
						$spec = "Fire";
						break;
					case "62 0 ":
						$spec = "Arcane";
						break;
					case "64 0 ":
						$spec = "Frost";
						break;
				}
			}
			if ($class == 9) {
				//Warlock
				switch ($s) {
					default:
						$spec = "";
						break;
					case "265 0 ":
						$spec = "Affliction";
						break;
					case "266 0 ":
						$spec = "Demonology";
						break;
					case "267 0 ":
						$spec = "Destruction";
						break;
				}
			}
			if ($class == 10) {
				//monk
				switch ($s) {
					default:
						$spec = "";
						break;
					case "270 0 ":
						$spec = "Mistweaver";
						break;
					case "268 0 ":
						$spec = "Brewmaster";
						break;
					case "269 0 ":
						$spec = "Windwalker";
						break;
				}
			}
			if ($class == 11) {
				//druid
				switch ($s) {
					default:
						$spec = "";
						break;
					case "102 0 ":
						$spec = "Balance";
						break;
					case "103 0 ":
						$spec = "Feral";
						break;
					case "104 0 ":
						$spec = "Guardian";
						break;
					case "105 0 ":
						$spec = "Restoration";
						break;
				}
			}

			return $spec;
		});
		$template->registerHelper('getCommentsCount', function ($s) {
			$comments = $this->commentsRepository->getCommentsByArticle($s);
			if ($comments) {
				return $comments->count("id");
			} else {
				$comments = NULL;
			}
		});
		$template->registerHelper('getRaceText', function ($s) {
			switch ($s) {
				default:
					$race = "Unknown";
					break;
				case 1:
					$race = "Human";
					break;
				case 2:
					$race = "Orc";
					break;
				case 3:
					$race = "Dwarf";
					break;
				case 4:
					$race = "Night Elf";
					break;
				case 5:
					$race = "Undead";
					break;
				case 6:
					$race = "Tauren";
					break;
				case 7:
					$race = "Gnome";
					break;
				case 8:
					$race = "Troll";
					break;
				case 9:
					$race = "Goblin";
					break;
				case 10:
					$race = "Blood Elf";
					break;
				case 11:
					$race = "Draenei";
					break;
				case 22:
					$race = "Worgen";
					break;
				case 24:
					$race = "Pandaren";
					break;
				case 25:
					$race = "Pandaren";
					break;
				case 26:
					$race = "Pandaren";
					break;
			}

			return $race;
		});

		$template->registerHelper('getClassText', function ($s) {
			switch ($s) {
				default:
					$class = "Unknown";
					break;
				case 1:
					$class = "Warrior";
					break;
				case 2:
					$class = "Paladin";
					break;
				case 3:
					$class = "Hunter";
					break;
				case 4:
					$class = "Rogue";
					break;
				case 5:
					$class = "Priest";
					break;
				case 6:
					$class = "Death Knight";
					break;
				case 7:
					$class = "Shaman";
					break;
				case 8:
					$class = "Mage";
					break;
				case 9:
					$class = "Warlock";
					break;
				case 10:
					$class = "Monk";
					break;
				case 11:
					$class = "Druid";
			}

			return $class;
		});


		$template->registerHelper('getMoney', function ($s) {
			if ($s) {
				/*
				 * int[] result = new int[3];
        int copper = m % 100;
        m = (m - copper) / 100;
        int silver = m % 100;

        int gold = (m - silver) / 100;
        result[0] = copper;
        result[1] = silver;
        result[2] = gold;
        return result;
				 */

				$copper = $s % 100;
				$silver = ($s - $copper) / 100;
				$silver = $s % 100;
				$gold = ($s - $silver) / 100;

				return $gold . "g " . $silver . "s " . $copper . "c";
			} else {
				return FALSE;
			}
		});

		$template->registerHelper('getTimeFromSeconds', function ($s) {
			if ($s) {
				$seconds = $s;
				$days = floor($seconds / 86400);
				$hours = floor($seconds / 3600);
				$mins = floor(($seconds - ($hours * 3600)) / 60);
				$secs = floor($seconds % 60);

				if ($days) {
					return $days . "d " . $hours . "h " . $mins . "m " . $secs . "s";
				}
				else {
					return $hours . "h " . $mins . "m " . $secs . "s";
				}
			} else {
				return FALSE;
			}
		});

		$template->registerHelper('getCharacterName', function ($s) {
			//guid
			$character = $this->charactersRepository->getCharacter($s);
			if ($character) {
				return $character->name;
			} else {
				return FALSE;
			}
		});

		$template->registerHelper('getCharacterPicture', function ($s) {
			//guid
			$character = $this->charactersRepository->getCharacter($s);
			if ($character) {
				return $character->race.$character->gender;
			} else {
				return FALSE;
			}
		});

		$template->registerHelper('getVideoId', function ($s) {
			parse_str(parse_url($s, PHP_URL_QUERY), $url);
			if ($url) {
				return $url['v'];
			} else {
				return FALSE;
			}
		});

		return $template;
	}



	function MemoryUsage()
	{
		foreach (file('/proc/meminfo') as $ri) {
			$m[strtok($ri, ':')] = strtok('');
		}

		return 100 - round(($m['MemFree'] + $m['Buffers'] + $m['Cached']) / $m['MemTotal'] * 100);
	}



	function MemoryUsageLinuxOsx()
	{
		$sys_load = sys_getloadavg();

		return round($sys_load[0] + $sys_load[1] + $sys_load[2]);
	}



	function getServerStatus()
	{
		$Fp1 = @fsockopen($this->context->parameters["ip"], $this->context->parameters["port"], $ERROR_NO, $ERROR_STR, (float) 2);
		if ($Fp1) {
			fclose($Fp1);

			return "Online";
		} else {
			//echo($ERROR_NO.','.$ERROR_STR);
			return "Offline";
		}
		restore_error_handler();
	}

}
