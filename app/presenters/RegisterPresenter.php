<?php


namespace App\Presenters;

use Brabijan\Images\TImagePipe;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette;
use Nette\Forms\Controls;


class RegisterPresenter extends BasePresenter
{

	/** @var  \App\Auth\AccountRepository @autowire */
	public $AccountRepository;

	/** @var  Nette\Http\Request @autowire */
	public $request;

	use TImagePipe;



	public function createComponentRegisterForm()
	{
		$form = new Form;

		$form->addText('email_verification', NULL)
			 ->setDefaultValue('your@email.com <mailto:your@email.com>')
			 ->addRule(~Form::FILLED, 'Kontrola proti botum, smazte toto pole')
			 ->setAttribute('class', 'emailVerification');

		$form->addText('email', 'Email:')
			 ->setType("email")
			 ->addRule(FORM::EMAIL)
			 ->setRequired('Email musí být vyplněn')
			 ->addRule(function ($control) {
				 $value = $control->getValue();

				 return $this->AccountRepository->isEmailAvailable($value);
			 }, "Email je již použit");

		$form->addText("username", "Uživatelské jméno:")
			 ->setRequired("Uživatelské jméno musí být vyplněno")
			 ->addRule(function ($control) {
				 $value = $control->getValue();

				 return $this->AccountRepository->isUsernameAvailable($value);
			 }, "Uživatelské jméno je již použito");


		$password1 = $form->addPassword("password1", "Heslo")
						  ->addRule(Form::FILLED, "Vyplňte heslo");

		$form->addPassword("password2", "Heslo znovu")
			 ->addConditionOn($form["password1"], Form::FILLED)
			 ->addRule(Form::EQUAL, "Hesla se neshodují", $form["password1"]);

		$password1->addConditionOn($form["password2"], Form::FILLED)
				  ->addRule(Form::FILLED, "Vyplňte heslo znovu");

		$form->addSubmit("send", "Registruj se!")
			 ->setAttribute("class", "btn btn-success");
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->registerFormSucceeded;

		return $form;
	}



	public function registerFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->AccountRepository->addUser($values->username, $values->email, $values->password1);
		$this->flashMessage("Byl(a) jste zaregistrován(a)!", "success");
		$this->redirect("Homepage:default");
	}
} 