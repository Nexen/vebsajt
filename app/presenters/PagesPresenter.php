<?php


namespace App\Presenters;


class PagesPresenter extends BasePresenter
{

	public function actionView($id, $webalized)
	{
		$this->template->page = $this->pagesRepository->getPage($id);
	}
} 