<?php


namespace App\Presenters;


class SecuredPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();
		if (!$this->user->isLoggedIn()) {
			$this->flashMessage("Nejste přihlášen(a)!", "danger");
			$this->redirect(":Homepage:default");
		} elseif (!$this->user->isAllowed($this->name, $this->action)) {
			$name = lcfirst(strtr($this->name, array(":" => "")));
			$this->forward(':PermissionDenied:' . $name);
		}
	}

} 