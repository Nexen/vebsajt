<?php

namespace App\Presenters;

use App\Passwords;
use App\UserManager;
use Nette;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl(a) jste odhlášen(a)!');
		$this->redirect('Homepage:default');
	}

}
