<?php


namespace App\Presenters;


use App\Passwords;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Brabijan;
use Nette;

class ProfilePresenter extends SecuredPresenter
{

	/** @var  \App\Auth\AccountRepository @autowire */
	public $AccountRepository;

	/** @var  \App\Auth\AvatarsRepository @autowire */
	public $AvatarsRepository;

	/** @var  \App\Characters\CharactersRepository @autowire */
	public $CharactersRepository;

	/** @var  Nette\Http\Request @autowire */
	public  $request;

	use Brabijan\Images\TImagePipe;



	public function renderDefault()
	{
		$this->template->profileUser = $this->AccountRepository->getUser($this->user->getId());
		$this->template->profilePicture = $this->AvatarsRepository->getPictureByUser($this->user->id);
		$this->template->recruiter = $this->AccountRepository->getUser($this->template->profileUser->recruiter);
		$this->template->characters = $this->CharactersRepository->getCharactersByAccount($this->user->getId());
		$this->template->webAdress = $this->request->getUrl();
	}



	public function renderView($id)
	{
		$this->template->profileUser = $this->AccountRepository->getUser($id);
		if ($this->template->profileUser) {
			$this->template->recruiter = $this->AccountRepository->getUser($this->template->profileUser->recruiter);
			$this->template->characters = $this->CharactersRepository->getCharactersByAccount($id);
			$this->template->webAdress = $this->request->getUrl();
			$this->template->profilePicture = $this->AvatarsRepository->getPictureByUser($id);
		} else {
			$this->setView("notFound");
		}
	}



	public function createComponentEditPictureForm()
	{
		$form = new Form();
		$form->addUpload("profilePicture", "Profilový obrázek")
			 ->setAttribute("class", "form-control")
			 ->addRule(Form::FILLED, "musíte nahrát obrázek");

		$form->addSubmit("send", "Nahrát")
			 ->setAttribute("class", "btn-primary");

		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->editPictureFormSucceeded;

		return $form;
	}



	public function editPictureFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->AvatarsRepository->addPicture($this->user->id, $values->profilePicture);
		$this->flashMessage("Profilový obrázek byl nahrán", "success");
		$this->redirect("this");
	}



	protected function createComponentChangePasswordForm()
	{
		$form = new Form();

		$form->addPassword("password_actual", "Aktuální heslo")
			 ->setAttribute("class", "form-control")
			 ->addRule(Form::FILLED, "Vyplňte aktuální heslo")
			 ->addRule(function ($control) {
				 $value = $control->getValue();

				 return $this->AccountRepository->hasValidPassword($this->user->identity->username, $value);
			 }, "Napište správné heslo");
		$password1 = $form->addPassword("password1", "Nové heslo")
						  ->setAttribute("class", "form-control");

		$form->addPassword("password2", "Nové heslo znovu")
			 ->setAttribute("class", "form-control")
			 ->addConditionOn($form["password1"], Form::FILLED)
			 ->addRule(Form::EQUAL, "Hesla se neshodují", $form["password1"])
			 ->addConditionOn($form["password_actual"], Form::FILLED)
			 ->addRule(Form::FILLED, "Aktuální heslo není vyplněno", $form["password_actual"]);

		$password1->addConditionOn($form["password2"], Form::FILLED)
				  ->addRule(Form::FILLED, "Vyplňte heslo znovu");

		$form->addSubmit('send', 'Změnit heslo');
		$form->onSuccess[] = $this->ChangePasswordFormSucceeded;
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());

		return $form;
	}



	public function ChangePasswordFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->AccountRepository->changeUserPassword($this->user->id, Passwords::makeShaHash($this->user->identity->username, $values->password1));
		$this->flashMessage("Heslo bylo úspěšně změněno", "success");
		$this->redirect('this');
	}



	protected function createComponentChangeEmailForm()
	{
		$form = new Form();

		$form->addText("email_actual", "Aktuální email")
			 ->setAttribute("class", "form-control")
			 ->addRule(Form::FILLED, "Vyplňte aktuální email")
			 ->addRule(function ($control) {
				 $value = $control->getValue();

				 return $this->AccountRepository->hasValidEmail($this->user->identity->username, $value);
			 }, "Napište správný email");
		$password1 = $form->addText("email1", "Nový email")
						  ->setAttribute("class", "form-control");

		$form->addText("email2", "Nový email znovu")
			 ->setAttribute("class", "form-control")
			 ->addConditionOn($form["email1"], Form::FILLED)
			 ->addRule(Form::EQUAL, "Emaily se neshodují", $form["email1"])
			 ->addConditionOn($form["email_actual"], Form::FILLED)
			 ->addRule(Form::FILLED, "Aktuální email není vyplněn", $form["email_actual"]);

		$password1->addConditionOn($form["email2"], Form::FILLED)
				  ->addRule(Form::FILLED, "Vyplňte email znovu");

		$form->addSubmit('send', 'Změnit email');

		$form->onSuccess[] = $this->ChangeEmailFormSucceeded;
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		return $form;
	}



	public function ChangeEmailFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->AccountRepository->changeUserEmail($this->user->id, $values->email1);
		$this->flashMessage("Email byl úspěšně změněn", "success");
		$this->redirect('this');
	}
}