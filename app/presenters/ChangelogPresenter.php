<?php


namespace App\Presenters;


use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use App;

class ChangelogPresenter extends SecuredPresenter
{

	/** @var  App\Auth\ChangelogRepository @autowire */
	public $changelogRepository;



	public function actionView($id)
	{
		$this->template->record = $this->changelogRepository->getRecord($id);
	}


	public function actionArchive()
	{
		$records = $this->changelogRepository->getChangelog();
		$recordsCount = $this->changelogRepository->getChangelog()->count();

		/** @var \NasExt\Controls\VisualPaginator $vp */
		$vp = $this['vp'];
		$paginator = $vp->getPaginator();
		$paginator->itemsPerPage = 20;
		$paginator->itemCount = $recordsCount;
		$records->limit($paginator->itemsPerPage, $paginator->offset);

		$this->template->records = $records;
	}



	/**
	 * @return \NasExt\Controls\VisualPaginator
	 */
	protected function createComponentVp($name)
	{
		$control = new \NasExt\Controls\VisualPaginator($this, $name);
		// enable ajax request, default is false
		$control->setAjaxRequest();

		$that = $this;
		$control->onShowPage[] = function ($component, $page) use ($that) {
			if ($that->isAjax()) {
				$that->redrawControl();
			}
		};

		return $control;
	}



	public function createComponentAddRecordForm()
	{
		$form = new Form();
		$milestones = array(
			"Quest" => "Quest",
			"Spell" => "Spell",
			"NPC" => "NPC",
			"Glitch" => "Glitch",
			"Character" => "Character",
			"Web" => "Web"
		);
		$form->addSelect("milestone", "Sekce:", $milestones)
			 ->setPrompt("Vyberte sekci");
		$form->addText("title", "Title: ")
			 ->setAttribute("class", "form-control");
		$form->addText("image", "Obrázek: ")
			 ->setAttribute("class", "form-control");
		$form->addText("video", "Video: ")
			 ->setAttribute("class", "form-control");
		$form->addTextArea("description", "Popis")
			 ->setAttribute("style", "width: 484px;height: 184px;")
			 ->setAttribute("id", "bugtrackerEditor");


		$form->addSubmit("send", "Odeslat");
		$form->onSuccess[] = $this->addRecordFormSucceeded;
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());

		return $form;
	}



	public function addRecordFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->changelogRepository->addRecord($values->milestone, $values->title, $values->description, $this->user->id, $values->image, $values->video);
		$this->flashMessage("Záznam byl úspěšně přidán", "success");
		$this->redirect("this");
	}

	public function handleDeleteRecord($id){
		$this->changelogRepository->deleteRecord($id);
		$this->flashMessage("Záznam byl úspěšně smazán", "success");
		$this->redirect(":Homepage:default");
	}
} 