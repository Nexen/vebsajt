<?php


namespace App\Presenters;


use Brabijan\Images\TImagePipe;
use App;
use Nette;

class CharacterPresenter extends BasePresenter
{

    use TImagePipe;

    /** @var  App\Characters\CharactersRepository @autowire */
    public $charactersRepository;

    /** @var  Nette\Database\Table\ActiveRow */
    public $selectedCharacter;

    /** @var  App\Characters\Character_EquipmentSetsRepository @autowire */
    public $characterEquipmentData;

    /** @var  App\Characters\Character_InventoryRepository @autowire */
    public $character_Inventory;

    /** @var  App\Characters\Item_InstanceRepository @autowire */
    public $item_instance;

    /** @var  App\World\item_templateRepository @autowire */
    public $item_template;

    /** @var  Nette\Http\Request @autowire */
    public $request;

    /** @var App\Characters\Character_currencyRepository @autowire */
    public $character_currencyRepository;

    /** @var App\Characters\Guild_memberRepository @autowire */
    public $guild_memberRepository;

    /** @var App\Characters\GuildRepository @autowire */
    public $guildRepository;

    /** @var App\Characters\Character_achievementRepository @autowire */
    public $character_achievementRepository;


    public function renderView($id, $name)
    {
        $this->template->webAdress = $this->request->getUrl();
        $this->selectedCharacter = $this->charactersRepository->getCharacter($id);
        if ($this->selectedCharacter) {
            $this->template->character = $this->selectedCharacter;
            $this->template->icons = $this->getArmory($id);
            $this->template->topHonors = $this->character_currencyRepository->getHonorsByGuid($id);

            $guildmember = $this->guild_memberRepository->getGuildIdByCharacter($id);
            if ($guildmember) {
                $guild = $this->guildRepository->getGuild($guildmember->guildid);
                $this->template->guild = $guild;
            }
            $this->template->achievementsCount = $this->character_achievementRepository->getAchievementsCount($id);
        } else {
            $this->setView("notFound");
        }
    }


    public function createComponentCharacterRender()
    {
        $charactersEquipmentData = $this->characterEquipmentData->getEquipmentForCharacter($this->selectedCharacter);

        return new App\Components\CharacterRender($this->selectedCharacter, $charactersEquipmentData, $this->character_Inventory, $this->item_instance, $this->item_template);
    }


    public function getArmory($guid)
    {
        $icons = array();

        foreach ($this->character_Inventory->getItems($guid) as $items) {
            foreach ($this->item_instance->getItemEntry($items->item) as $itemEntry) {
                $icons[$itemEntry->itemEntry] = $this->getIcon($itemEntry->itemEntry);
            }
        }

        return $icons;

    }


    public function getIcon($itemId)
    {
        if ($itemId == NULL) {
            return;
        }

        $storage = new Nette\Caching\Storages\FileStorage(__CACHE_DIR__.'/itemsIcons');
        $cache = new Nette\Caching\Cache($storage);

        $value = $cache->load($itemId);

        if ($value === NULL) {
            $url = 'http://www.wowhead.com/item=' . $itemId . '&xml';

            $ch = curl_init($url);
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $response = curl_exec($ch);
            $p = xml_parser_create();
            xml_parse_into_struct($p, $response, $vals, $index);
            xml_parser_free($p);
            $position = strstr(strstr($vals[$index['JSON'][0]]['value'], 'slotbak":'), ',', TRUE);
            $position = substr(strstr($position, ':'), 1);
            $position = trim($position);


            $result = $vals[$index['ICON'][0]]['value'];
            $cache->save($itemId, $result);

            return $result;
        } else {
            return $value;
        }
    }

}