<?php

namespace App\Presenters;

use Brabijan\Images\TImagePipe;
use Nette;
use App;
use App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	/** @var  App\Web\ArticlesRepository @autowire */
	public $ArticlesRepository;

	/** @var App\Auth\SliderRepository @autowire*/
	public $sliderRepository;

	/** @var App\Auth\CommentsRepository @autowire*/
	public $commentsRepository;

	/** @var App\Characters\CharactersRepository @autowire*/
	public $charactersRepository;

	/** @var App\Characters\Character_currencyRepository @autowire*/
	public $character_currencyRepository;

	use TImagePipe;



	public function renderDefault()
	{
		$this->template->lastArticles = $this->ArticlesRepository->getArticles()->order("articles.publishDate DESC")->limit(2);
		$this->template->articles = $this->ArticlesRepository->getArticles()->order("articles.publishDate DESC")->limit(6, 2);
		$this->template->slides = $this->sliderRepository->getSlides()->order("order ASC");
		$this->template->topKills = $this->charactersRepository->getTopKills(5);
		$this->template->topUptimes = $this->charactersRepository->getTopUptime(5);
		$this->template->topHonors = $this->character_currencyRepository->getTopHonored(5);
	}

}
