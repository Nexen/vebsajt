<?php

namespace App\Presenters;


use Nette\Application\UI\Form;
use App;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Strings;
use Nextras\Forms\Rendering\Bs3FormRenderer;

class LostPasswordPresenter extends BasePresenter
{

	/** @var App\Auth\AccountRepository @autowire */
	public $accountRepository;

	public function createComponentLostPasswordForm()
	{
		$form = new Form();

		$form->addText("username", "Jméno účtu")
			 ->addRule(Form::FILLED, "Vyplte jméno účtu");

		$form->addSubmit("send", "Obnovit heslo");
		$form->onSuccess[] = $this->lostPasswordFormSucceeded;
		$form->setRenderer(new Bs3FormRenderer());

		return $form;
	}

	public function lostPasswordFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$account = $this->accountRepository->getUserByUsername($values->username);
		$randomPass = Strings::random(5, "A-Z");
		$password = App\Passwords::makeShaHash($values->username, $randomPass);
		$this->accountRepository->setPassword($account->id, $password);

		$mail = new Message();
		$mail->setFrom('info@gameshoot.eu')
			 ->addTo($account->email)
			 ->setSubject('Black Death Reset Hesla')
			 ->setBody("Dobrý den,\nvaše heslo je nyní: {$password}\nMůžete si jej změnit v profilu nebo ve hře příkazem .account password.");
		$mailer = new SendmailMailer();
		$mailer->send($mail);

		$this->flashMessage("Heslo vám bylo posláno na adresu: {$this->mask_email($account->email, "*", 50)}");
		$this->redirect("Homepage:default");
	}



	function mask_email($email, $mask_char, $percent = 50)
	{

		list($user, $domain) = preg_split("/@/", $email);
		$len = strlen($user);
		$mask_count = floor($len * $percent / 100);
		$offset = floor(($len - $mask_count) / 2);
		$masked = substr($user, 0, $offset)
			. str_repeat($mask_char, $mask_count)
			. substr($user, $mask_count + $offset);

		return ($masked . '@' . $domain);
	}
} 