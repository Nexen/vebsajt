<?php


namespace App\Presenters;

use App;
use Brabijan\Images\TImagePipe;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;

class ArticlePresenter extends BasePresenter
{

    /** @var  App\Web\ArticlesRepository @autowire */
    public $ArticlesRepository;


    /** @var App\Auth\CommentsRepository @autowire */
    public $CommentsRepository;

    /** @var  integer */
    private $selectedArticleId;

    use TImagePipe;


    public function actionView($id, $webalized)
    {
        $this->selectedArticleId = $id;
        $this->template->article = $this->ArticlesRepository->getArticle($id);
        $this->template->comments = $this->CommentsRepository->getCommentsByArticle($id);
    }


    public function createComponentAddCommentForm()
    {
        $form = new Form();

        $form->addTextArea("text", "Komentář", NULL, 5)
            ->setAttribute("placeholder", "Napište komentář")
            ->setAttribute("id", "ckEditor");

        $form->addSubmit("send", "Přidat komentář");
        $form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
        $form->onSuccess[] = $this->addCommentFormSucceeded;

        return $form;
    }


    public function addCommentFormSucceeded(Form $form)
    {
        $values = $form->getValues();

        $this->CommentsRepository->addComment($this->selectedArticleId, $values->text, $this->user->id);
        $this->flashMessage("Váš komentář byl přidán", "success");
        $this->redirect("this");
    }


    public function handleDeleteComment($commentId, $articleId)
    {
        $this->CommentsRepository->deleteComment($commentId);
        $this->flashMessage("Komentář byl smazán", "success");
        $this->redirect("this", $articleId);

    }

}