<?php


namespace App\ForumModule\presenters;

use App;
use Brabijan\Images\TImagePipe;

class TopicPresenter extends BasePresenter
{

	/** @var  App\Forum\PostsRepository @autowire */
	public $postsRepository;

	/** @var  App\Forum\CategoriesRepository @autowire */
	public $categoriesRepository;

	/** @var  App\Forum\TopicsRepository @autowire */
	public $topicsRepository;

	use TImagePipe;



	public function actionView($id, $webalized)
	{
		$posts = $this->postsRepository->getPostsByCategory($id);

		$tmp = $posts;

		$post = $tmp->fetch();

		$topic = $post->ref("topics","topicId");
		$category = $topic->ref("categories", "categoryId");
		$parent = $category->ref("categories", "parentId");

		$this->template->topic = $topic;
		$this->template->category = $category;
		$this->template->parent = $parent;
		$this->template->posts = $posts;
	}
} 