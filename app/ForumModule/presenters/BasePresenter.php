<?php

namespace App\ForumModule\presenters;

use Nette;
use App;

class BasePresenter extends Nette\Application\UI\Presenter
{

	use \Kdyby\Autowired\AutowireProperties;
	use \Kdyby\Autowired\AutowireComponentFactories;


	/** @var  App\Auth\AccountRepository @autowire */
	public $accountRepository;


	/** @var  App\Auth\AvatarsRepository @autowire */
	public $AvatarsRepository;

	/** @var  App\Auth\PagesRepository @autowire */
	public $pagesRepository;

	/** @var  App\Forum\PostsRepository @autowire */
	public $postsRepository;



	public function beforeRender()
	{
		$this->template->pages = $this->pagesRepository->getPages();
	}



	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;
		$lostPassword = Nette\Utils\Html::el("a", "link text")->href("somewhere")->class("positionMe");

		$form->addText('username', 'Uživatelské jméno: ')
			 ->setRequired('Please enter your username.')
			 ->setAttribute("class", "form-control");

		$form->addPassword('password', 'Heslo: ')
			 ->setRequired('Please enter your password.')
			 ->setAttribute("class", "form-control");

		$form->addCheckbox('remember', 'Zapamatovat si přihlášení');
		$link = \Nette\Utils\Html::el("a", array("href" => $this->link(":LostPassword:default")))->setHtml("Zapomenuté heslo?");
		$form->addCheckbox("lostPassword", $link)
			 ->setAttribute("style", "display:none");

		$form->addSubmit('send', 'Přihlásit')
			 ->setAttribute("class", "btn btn-primary");

		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->signInFormSucceeded;

		return $form;
	}



	public function signInFormSucceeded($form)
	{
		$values = $form->getValues();

		if ($values->remember) {
			$this->getUser()->setExpiration('14 days', FALSE);
		} else {
			$this->getUser()->setExpiration('20 minutes', TRUE);
		}

		try {
			$this->getUser()->login($values->username, $values->password);
			$this->flashMessage("Byl(a) si přihlášen(a)!", "success");
			$this->redirect('this');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
			$this->flashMessage("Špatné jméno nebo heslo, zapomněli jste heslo?", "danger");
		}
	}



	protected function createTemplate($class = NULL)
	{
		$template = parent::createTemplate($class);
		$template->registerHelper('getAuthorName', function ($s) {
			$author = $this->accountRepository->getUser($s);
			if ($author) {
				return $author->username;
			} else {
				return $author = NULL;
			}
		});
		$template->registerHelper('getAuthorPicture', function ($s) {
			$avatar = $this->AvatarsRepository->getPictureByUser($s);
			if ($avatar) {
				return $avatar->avatar;
			} else {
				return FALSE;
			}
		});
		$template->registerHelper('getPostsCount', function ($s) {
			$postsCount = $this->postsRepository->getPostsByCategory($s)->count("id");
			if ($postsCount) {
				return $postsCount;
			} else {
				return FALSE;
			}
		});

		//@todo dodělat!
		/*
		$template->registerHelper('getAuthorInfo', function ($s) {
			$author = $this->accountRepository->getUser($s);
			if ($author) {
				return $authorInfo = array(
					""
				);
			} else {
				return $author = NULL;
			}
		});*/

		return $template;
	}
} 