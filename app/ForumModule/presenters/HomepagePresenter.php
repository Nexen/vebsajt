<?php

namespace App\ForumModule\presenters;

use App;

class HomepagePresenter extends BasePresenter
{

	/** @var  App\Forum\CategoriesRepository @autowire */
	public $categoriesRepository;

	/** @var  App\Forum\SubCategoriesRepository @autowire */
	public $subCategoriesRepository;




	public function renderDefault()
	{
		$this->template->categories = $this->categoriesRepository->getAllCategoriesWithSubs();
	}
} 