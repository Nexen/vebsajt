<?php


namespace App\ForumModule\presenters;

use App;

class CategoryPresenter extends BasePresenter
{

	/** @var  App\Forum\CategoriesRepository @autowire */
	public $categoriesRepository;

	/** @var  App\Forum\TopicsRepository @autowire */
	public $topicsRepository;



	public function actionView($id, $parentCat = NULL, $webalized)
	{
		$selectedCategory = $this->categoriesRepository->getCategory($id);

		if($selectedCategory->parentId != NULL){
			$parent = $this->categoriesRepository->getCategory($selectedCategory->parentId);
			$this->template->parentCategory = $parent;
		}

		$this->template->selectedCategory = $selectedCategory;

		$this->template->topics = $this->topicsRepository->getTopicsByCategory($id);
	}

} 