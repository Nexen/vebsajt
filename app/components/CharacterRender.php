<?php


namespace App\Components;


use Nette\Application\UI\Control;
use Nette;
use App;

class CharacterRender extends Control
{

	/** @var \Nette\Database\Context */
	private $character;

	/** @var \Nette\Database\Table\Selection */
	private $charactersData;

	/** @var App\Characters\Character_InventoryRepository */
	private $character_inventory;

	/** @var App\Characters\Item_InstanceRepository */
	private $item_instance;

	/** @var App\World\item_templateRepository */
	private $item_template;



	public function __construct(
		Nette\Database\Table\ActiveRow $character,
		Nette\Database\Table\Selection $charactersData,
		App\Characters\Character_InventoryRepository $character_inventory,
		App\Characters\Item_InstanceRepository $item_instance,
		App\World\item_templateRepository $item_template)
	{
		$this->character = $character;
		$this->charactersData = $charactersData;
		$this->character_inventory = $character_inventory;
		$this->item_instance = $item_instance;
		$this->item_template = $item_template;
	}



	public function render()
	{
		$this->template->setFile(__DIR__ . "/characterRender.latte");
		$this->template->charactersData = $this->getModelScene($this->character);
		$this->template->character = $this->character;
		$this->template->render();
	}



	public function getDisplayId($itemId)
	{
		if ($itemId == NULL) {
			return;
		}

		$storage = new Nette\Caching\Storages\FileStorage(__CACHE_DIR__.'/items');
		$cache = new Nette\Caching\Cache($storage);

		$value = $cache->load($itemId);

		if ($value === NULL) {
			/*if (property_exists($item, 'tooltipParams') && property_exists($item->tooltipParams, 'transmogItem')) {
				$id = $item;
			} else {
				$id = $item;
			}*/
			$url = 'http://www.wowhead.com/item=' . $itemId . '&xml';

			$ch = curl_init($url);
			$timeout = 5;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			$response = curl_exec($ch);
			$p = xml_parser_create();
			xml_parse_into_struct($p, $response, $vals, $index);
			xml_parser_free($p);
			$position = strstr(strstr($vals[$index['JSON'][0]]['value'], 'slotbak":'), ',', TRUE);
			$position = substr(strstr($position, ':'), 1);
			$position = trim($position);

			$result = $position . ',' . $vals[$index['ICON'][0]]['attributes']['DISPLAYID'];

			$cache->save($itemId, $result);
			return $result;
		}
		else {
			return $value;
		}
	}



	public function getModelScene($character)
	{
		$guid = $character->guid;
		$race = $character->race;
		$gender = $character->gender;
		$b = $character->playerBytes;
		$b2 = $character->playerBytes2;

		// Set Character Features
		$ha = ($b >> 16) % 256;
		$hc = ($b >> 24) % 256;
		$fa = ($b >> 8) % 256;
		$sk = $b % 256;
		$fh = $b2 % 256;

		// Set Character Race/Gender
		$char_race = array(
			1 => 'human',
			2 => 'orc',
			3 => 'dwarf',
			4 => 'nightelf',
			5 => 'scourge',
			6 => 'tauren',
			7 => 'gnome',
			8 => 'troll',
			10 => 'bloodelf',
			11 => 'draenei',
			22 => 'worgen',
			24 => 'pandaren',
			25 => 'pandaren',
			26 => 'pandaren');

		$char_gender = array(
			0 => 'male',
			1 => 'female');

		$rg = $char_race[$race] . $char_gender[$gender];

		$eq = "";
		$icons = array();
		foreach ($this->character_inventory->getItems($guid) as $items) {
			foreach ($this->item_instance->getItemEntry($items->item) as $itemEntry) {
				if ($eq == "") {
					$eq = $this->getDisplayId($itemEntry->itemEntry);
				} else {
					$eq .= "," . $this->getDisplayId($itemEntry->itemEntry);
				}
			}
		}


		return $model = array(
			"rg" => $rg,
			"ha" => $ha,
			"hc" => $hc,
			"fa" => $fa,
			"sk" => $sk,
			"fh" => $fh,
			"eq" => $eq
		);
	}
} 