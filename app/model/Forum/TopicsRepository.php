<?php


namespace App\Forum;


use App\BaseRepository;

class TopicsRepository extends BaseRepository
{

	public function getTopicsByCategory($id)
	{
		return $this->findBy(array(
			"categoryId" => $id,
		));
	}



	public function getParentThread($id)
	{
		return $this->find($id);
	}

	public function getSubThread($id)
	{
		return $this->findBy(array(
			"threads_id" => $id
		));
	}
} 