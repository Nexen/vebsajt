<?php


namespace App\Forum;


use App\BaseRepository;
use App\Passwords;

class CategoriesRepository extends BaseRepository
{

	private $categories;
	private $categoriesTree;



	public function getAllCategories()
	{
		return $this->findAll()->order("order ASC");
	}



	public function getAllCategoriesByParent($parentId)
	{
		return $this->findBy(array(
			"parentId" => $parentId
		))->order("order ASC");
	}



	public function getCategory($id)
	{
		return $this->find($id);
	}



	public function getAllCategoriesWithSubs($subtree = NULL)
	{
		if ($subtree == NULL) {
			foreach ($this->getAllCategories() as $category) {
				$this->categories[] = $category;
			}
		}

		$categories = array();
		if (!empty($this->categories)) {
			foreach ($this->categories as $category) {
				if ($category["parentId"] == $subtree) {
					$tmp = $category->toArray();
					$tmp["data"] = $category;
					$tmp["subCategories"] = $this->getAllCategoriesByParent($category["id"]);
					$categories[] = $tmp;
				}
			}
		}
		if ($subtree == NULL) {
			$this->categoriesTree = $categories;

			return $this->categoriesTree;
		}

		return $categories;
	}
} 