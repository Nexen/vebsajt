<?php


namespace App\Forum;


use App\BaseRepository;

class PostsRepository extends BaseRepository
{

	public function getPostsByCategory($id)
	{
		return $this->findBy(array(
			"topicId" => $id,
		));
	}
} 