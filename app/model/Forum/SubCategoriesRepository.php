<?php


namespace App\Forum;


use App\BaseRepository;

class SubCategoriesRepository extends BaseRepository
{

	public function getSubCategories($id)
	{
		return $this->findBy(array(
			"categoryId" => $id
		));
	}



	public function getSubCategory($id)
	{
		return $this->find($id);
	}
} 