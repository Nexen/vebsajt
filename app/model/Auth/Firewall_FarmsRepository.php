<?php


namespace App\Auth;


use App\BaseRepository;

class Firewall_FarmsRepository extends BaseRepository
{

	public function addIp($ip)
	{
		$data = array(
			"ip" => $ip,
			"type" => 0
		);

		$this->getTable()->insert($data);
	}

} 