<?php


namespace App\Auth;


use App\BaseRepository;
use App\Passwords;
use App\UserManager;
use Nette;

class AccountRepository extends BaseRepository
{

	/** @var  Nette\Http\Request */
	private $request;



	public function __construct(Nette\Database\Context $connection, Nette\Http\Request $request)
	{
		parent::__construct($connection);

		$this->request = $request;
	}



	public function getUser($id)
	{
		return $this->find($id);
	}



	public function addUser($username, $email, $password)
	{
		$data = array(
			"username" => $username,
			"sha_pass_hash" => Passwords::makeShaHash($username, $password),
			"email" => $email,
			"last_ip" => $this->request->getRemoteAddress(),
			"expansion" => 4,
			"role" => "member"
		);

		return $this->getTable()->insert($data);
	}



	public function isEmailAvailable($email)
	{
		return $this->findOneBy("email", $email) === FALSE;
	}



	public function isUsernameAvailable($username)
	{
		return $this->findOneBy("username", $username) === FALSE;
	}



	public function hasValidPassword($username, $password)
	{

		$row = $this->findOneBy("username", $username);

		return $row->sha_pass_hash === Passwords::makeShaHash($username, $password);
	}



	public function hasValidEmail($username, $email)
	{
		$row = $this->findOneBy("username", $username);

		return $row->email === $email;
	}



	public function changeUserPassword($id, $password)
	{
		return $this->find($id)->update(array(
			"sha_pass_hash" => $password
		));
	}



	public function changeUserEmail($id, $email)
	{
		return $this->find($id)->update(array(
			"email" => $email
		));
	}



	public function getUserByUsername($username)
	{
		return $this->findOneBy(array(
			"username" => $username
		));
	}



	public function setPassword($accountId, $password)
	{
		return $this->find($accountId)->update(array(
			"sha_pass_hash" => $password
		));
	}
} 