<?php


namespace App\Auth;


use App\BaseRepository;
use Nette;
use Brabijan;

class AvatarsRepository extends BaseRepository
{

	/** @var Brabijan\Images\ImageStorage */
	public $imageStorage;



	public function __construct(Nette\Database\Context $connection, Brabijan\Images\ImageStorage $imageStorage)
	{
		parent::__construct($connection);

		$this->imageStorage = $imageStorage;
	}



	public function getPicture($id)
	{
		return $this->find($id);
	}

	public function getPictureByUser($userId){
		return $this->findOneBy(array(
			"accountId" => $userId
		));
	}



	public function addPicture($userId, $profilePicture)
	{
		$row = $this->getPictureByUser($userId);
		$imageStorage = $this->imageStorage->setNamespace("avatars")->upload($profilePicture);

		if ($row == FALSE) {
			$data = array(
				"accountId" => $userId,
				"avatar" => basename($imageStorage->getFile())
			);

			return $this->getTable()->insert($data);
		} else {
			$this->imageStorage->setNamespace("avatars")->deleteFile($row->avatar);

			$data = array(
				"avatar" => basename($imageStorage->getFile())
			);

			return $this->getPictureByUser($userId)->update($data);
		}
	}

} 