<?php


namespace App\Auth;


use App\BaseRepository;

class ChangelogRepository extends BaseRepository
{

	public function getChangelog()
	{
		return $this->findAll();
	}



	public function addRecord($milestone, $title, $description, $author, $image = NULL, $video = NULL, $status = "Reported")
	{
		$data = array(
			"milestone" => $milestone,
			"title" => $title,
			"description" => $description,
			"image" => $image,
			"video" => $video,
			"author" => $author,
			"status" => $status
		);

		return $this->getTable()->insert($data);
	}



	public function updateRecord($id, $milestone, $title, $description, $image, $video, $author, $status = NULL)
	{
		$data = array(
			"milestone" => $milestone,
			"title" => $title,
			"description" => $description,
			"image" => $image,
			"video" => $video,
			"author" => $author,
			"status" => $status
		);

		return $this->find($id)->update($data);
	}



	public function getRecord($id)
	{
		return $this->find($id);
	}



	public function deleteRecord($id)
	{
		return $this->find($id)->delete();
	}
} 