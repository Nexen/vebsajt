<?php


namespace App\Auth;


use App\BaseRepository;
use Brabijan;
use Nette;

class SliderRepository extends BaseRepository
{

	/** @var Brabijan\Images\ImageStorage */
	public $imageStorage;



	public function __construct(Nette\Database\Context $connection, Brabijan\Images\ImageStorage $imageStorage)
	{
		parent::__construct($connection);

		$this->imageStorage = $imageStorage;
	}



	public function getSlides()
	{
		return $this->findAll();
	}



	public function getSlide($id)
	{
		return $this->find($id);
	}



	public function addSlide($slide)
	{
		$imageStorage = $this->imageStorage->setNamespace("slides")->upload($slide);

		$data = array(
			"slide" => basename($imageStorage->getFile())
		);

		$row = $this->getTable()->insert($data);

		return $this->find($row->id)->update(array(
			"order" => $row->id
		));
	}



	public function deleteSlide($id)
	{
		return $this->find($id)->delete();
	}



	public function moveUp($id)
	{
		$currentSlide = $this->find($id);
		$currentOrder = (int) $currentSlide->order;

		$previousSlide = $this->findBy(array("order < ?" => $currentOrder))->limit(1)->order("order DESC")->fetch();
		if ($previousSlide !== FALSE) {
			$currentSlide->update(array("order" => $previousSlide->order));
			$previousSlide->update(array("order" => $currentOrder));
		}

	}



	public function moveDown($id)
	{
		$currentSlide = $this->find($id);
		$currentOrder = (int) $currentSlide->order;

		$previousSlide = $this->findBy(array("order > ?" => $currentOrder))->limit(1)->order("order ASC")->fetch();
		if ($previousSlide !== FALSE) {
			$currentSlide->update(array("order" => $previousSlide->order));
			$previousSlide->update(array("order" => $currentOrder));
		}
	}
} 