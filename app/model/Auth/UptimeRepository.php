<?php


namespace App\Auth;


use App\BaseRepository;

class UptimeRepository extends BaseRepository
{

	public function getUptime()
	{
		$row = $this->findAll()->order("starttime DESC")->limit(1)->fetch();
		if($row){
			return $row->uptime;
		}
		else {
			return NULL;
		}
	}
} 