<?php


namespace App\Auth;


use App\BaseRepository;
use Nette\DateTime;
use Nette\Utils\Strings;

class PagesRepository extends BaseRepository
{

	public function getPages()
	{
		return $this->findAll();
	}



	public function getPage($id)
	{
		return $this->find($id);
	}



	public function addPage($title, $text, $author)
	{
		$data = array(
			"title" => $title,
			"text" => $text,
			"author" => $author,
			"publishDate" => new DateTime(),
			"webalized" => Strings::webalize($title)
		);

		return $this->getTable()->insert($data);
	}



	public function editPage($id, $title, $text, $author)
	{
		$data = array(
			"title" => $title,
			"text" => $text,
			"author" => $author,
			"editDate" => new DateTime(),
			"webalized" => Strings::webalize($title)
		);

		return $this->find($id)->update($data);
	}



	public function deletePage($id)
	{
		return $this->find($id)->delete();
	}
} 