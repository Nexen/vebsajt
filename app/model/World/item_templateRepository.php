<?php


namespace App\World;


use App\BaseRepository;

class item_templateRepository extends BaseRepository
{

	public function getItemTemplate($itemEntry)
	{
		return $this->findBy(array(
			"entry" => $itemEntry
		))->limit(1)->fetch();
	}
} 