<?php


namespace App\Auth;


use App\BaseRepository;
use Nette;

class CommentsRepository extends BaseRepository
{

	public function getCommentsByArticle($id)
	{
		return $this->findBy(array(
			"articleId" => $id
		));
	}



	public function addComment($articleId, $text, $author)
	{
		$data = array(
			"text" => $text,
			"articleId" => $articleId,
			"author" => $author,
			"publishDate" => new Nette\DateTime()
		);

		return $this->getTable()->insert($data);
	}



	public function deleteComment($id)
	{
		return $this->find($id)->delete();
	}
} 