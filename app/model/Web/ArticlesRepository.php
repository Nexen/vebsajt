<?php


namespace App\Web;


use App\BaseRepository;
use Brabijan;
use Nette;

class ArticlesRepository extends BaseRepository
{

	/** @var Brabijan\Images\ImageStorage */
	public $imageStorage;



	public function __construct(Nette\Database\Context $connection, Brabijan\Images\ImageStorage $imageStorage)
	{
		parent::__construct($connection);

		$this->imageStorage = $imageStorage;
	}



	public function getArticles()
	{
		return $this->findAll();
	}



	public function getArticle($id)
	{
		return $this->find($id);
	}



	public function addArticle($author, $thumbnail, $title, $text)
	{
		$imageStorage = $this->imageStorage->setNamespace("Articles")->upload($thumbnail);

		$data = array(
			"author" => $author,
			"title" => $title,
			"text" => $text,
			"publishDate" => new Nette\DateTime(),
			"thumbnail" => basename($imageStorage->getFile()),
			"webalized" => Nette\Utils\Strings::webalize($title)
		);

		return $this->getTable()->insert($data);
	}



	public function editArticle($id, $author, $thumbnail, $title, $text)
	{

		if ($thumbnail->name != NULL) {
			$imageStorage = $this->imageStorage->setNamespace("Articles")->upload($thumbnail);
			$data["thumbnail"] = basename($imageStorage->getFile());
		}

		$data = array(
			"author" => $author,
			"title" => $title,
			"text" => $text,
			"publishDate" => new Nette\DateTime(),
			"webalized" => Nette\Utils\Strings::webalize($title)
		);

		return $this->find($id)->update($data);
	}



	public function deleteArticle($id)
	{
		return $this->find($id)->delete();
	}
} 