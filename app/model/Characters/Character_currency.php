<?php


namespace App\Characters;


use App\BaseRepository;

class Character_currencyRepository extends BaseRepository
{

	public function getTopHonored($limit)
	{
		return $this->findBy(array(
			"currency" => 392
		))->order("Quantity DESC")->limit($limit);
	}

	public function getHonorsByGuid($guid)
	{
		return $this->findOneBy(array(
			"currency" => 392,
			"CharacterGuid" => $guid
		));
	}
} 