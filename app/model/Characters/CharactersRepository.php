<?php


namespace App\Characters;


use App\BaseRepository;

class CharactersRepository extends BaseRepository
{

	public function getCharactersByAccount($accountId)
	{
		return $this->findBy(array(
			"account" => $accountId
		));
	}



	public function getOnlineCharacters($fraction)
	{
		if ($fraction == "Alliance") {
			return $this->findBy(array(
				"online" => 1
			))          ->where("race IN (1, 3, 4, 7, 11, 22, 25)")->count();
		} elseif ($fraction == "Horde") {
			return $this->findBy(array(
				"online" => 1
			))          ->where("race IN (2, 5, 6, 8, 9, 10, 26)")->count();
		} else {
			return $this->findBy(array(
				"online" => 1
			))          ->where("race IN (24)")->count();
		}
	}



	public function getCharacter($guid)
	{
		return $this->findOneBy(array(
			"guid" => $guid,
			"deleteDate" => NULL
		));
	}



	/***/
	public function getTopKills($limit)
	{
		return $this->findAll()->order("totalKills DESC")->limit($limit);
	}

	public function getTopUptime($limit)
	{
		return $this->findAll()->order("totaltime DESC")->limit($limit);
	}

} 