<?php


namespace App\Characters;


use App\BaseRepository;

class Item_InstanceRepository extends BaseRepository
{

	public function getItemEntry($guid)
	{
		return $this->findBy(array(
			"guid" => $guid
		));
	}
} 