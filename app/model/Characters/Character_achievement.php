<?php


namespace App\Characters;


use App\BaseRepository;

class Character_achievementRepository extends BaseRepository
{

	public function getAchievementsCount($guid)
	{
		return $this->findBy(array(
			"guid" => $guid
		))->count();
	}
} 