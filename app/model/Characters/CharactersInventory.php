<?php


namespace App\Characters;


use App\BaseRepository;

class Character_InventoryRepository extends BaseRepository
{
	public function getItems($guid){
		return $this->findBy(array(
			"guid" => $guid,
			"bag" => 0,
			"slot < ?" => 18
		));
	}
} 