<?php


namespace App\Characters;


use App\BaseRepository;

class Character_EquipmentSetsRepository extends BaseRepository
{

	public function getEquipmentForCharacter($guid)
	{
		return $this->findBy(array(
			"guid" => $guid
		))->order("guid DESC")->limit(0, 1);
	}
}