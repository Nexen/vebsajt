<?php


namespace App\Characters;


use App\BaseRepository;

class Guild_memberRepository extends BaseRepository
{

	/***/
	public function getGuildIdByCharacter($guid)
	{
		return $this->findOneBy(array(
			"guid" => $guid
		));
	}
} 