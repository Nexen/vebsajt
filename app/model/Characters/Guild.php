<?php


namespace App\Characters;


use App\BaseRepository;

class GuildRepository extends BaseRepository
{

	public function getGuild($id)
	{
		return $this->find($id);
	}
} 