<?php

namespace App;

class Authorizator extends \Nette\Security\Permission
{

	public function __construct()
	{
		$this->addRole("guest");
		$this->addRole("member");

		$this->addRole("moderator");
		//$this->addRole("gamemaster");
		$this->addRole("developer");
		$this->addRole("admin");

		$this->addResource("Homepage");
		$this->addResource("Sign");
		$this->addResource("Register");
		$this->addResource("Profile");
		$this->addResource("Admin:Homepage");
		$this->addResource("Admin:Articles");
		$this->addResource("Admin:Slider");
		$this->addResource("Character");
		$this->addResource("Admin:Changelog");
		$this->addResource("Changelog");
		$this->addResource("Admin:Pages");
		$this->addResource("Pages");
		$this->addResource("Article");
		$this->addResource("Comments");
		$this->addResource("ChangelogAdmin");

		$this->deny(self::ALL, self::ALL, self::ALL);

		$this->allow("admin", self::ALL, self::ALL);

		$this->allow("guest", "Register", self::ALL);
		$this->allow("guest", "Sign", self::ALL);
		$this->allow("guest", "Pages", self::ALL);

		$this->allow("member", "Profile", self::ALL);
		$this->allow("member", "Changelog", self::ALL);
		$this->allow("member", "Article", self::ALL);

		$this->allow("moderator", "Profile", self::ALL);
		$this->allow("moderator", "Changelog", self::ALL);
		$this->allow("moderator", "Article", self::ALL);
		$this->allow("moderator", "Pages", self::ALL);
		$this->allow("moderator", "Sign", self::ALL);
		$this->allow("moderator", "Admin:Homepage", self::ALL);
		$this->allow("moderator", "Admin:Pages", self::ALL);
		$this->allow("moderator", "Comments", self::ALL);
		$this->allow("moderator", "ChangelogAdmin", self::ALL);

		$this->allow("developer", "Homepage", self::ALL);
		$this->allow("developer", "Profile", self::ALL);
		$this->allow("developer", "Pages", self::ALL);
		$this->allow("developer", "Sign", self::ALL);
		$this->allow("developer", "Changelog", self::ALL);
		$this->allow("developer", "Admin:Homepage", self::ALL);
		$this->allow("developer", "Admin:Changelog", self::ALL);
		$this->allow("developer", "Comments", self::ALL);
		$this->allow("developer", "ChangelogAdmin", self::ALL);
	}
}