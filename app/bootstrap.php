<?php

require __DIR__ . '/../vendor/autoload.php';

define("__CACHE_DIR__", __DIR__."/../temp/cache");

$configurator = new Nette\Configurator;

//$configurator->setDebugMode(TRUE);  // debug mode MUST NOT be enabled on production server
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
			 ->addDirectory(__DIR__)
			 ->addDirectory(__DIR__ . '/../vendor/others')
			 ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

\Kdyby\Autowired\DI\AutowiredExtension::register($configurator);
\Brabijan\Images\DI\ImagesExtension::register($configurator);

$container = $configurator->createContainer();

return $container;
