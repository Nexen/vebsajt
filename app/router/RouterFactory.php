<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
		$router[] = $adminRouter = new RouteList("Admin");
		$router[] = $forumRouter = new RouteList("Forum");
		$adminRouter[] = new Route("admin/<presenter>[/<action>][/<id>]", 'Homepage:default');

		$forumRouter[] = new Route('forum/kategorie[/<id>-][<parentCat>][-<webalized>]', array(
			'presenter' => "Category",
			'action' => array(
				Route::VALUE => 'view'
			)
		));

		$forumRouter[] = new Route('forum/tema[/<id>-<webalized>]', array(
			'presenter' => "Topic",
			'action' => array(
				Route::VALUE => 'view'
			)
		));

		$forumRouter[] = new Route("forum/<presenter>[/<action>][/<id>]", 'Homepage:default');

		$router[] = new Route('/clanek[/<id>-<webalized>]', array(
			'presenter' => "Article",
			'action' => array(
				Route::VALUE => 'view'
			)
		));
		$router[] = new Route('/bugtracker/pridani[/<id>]', array(
			'presenter' => "Changelog",
			'action' => array(
				Route::VALUE => 'add'
			)
		));
		$router[] = new Route('/bugtracker/<action>[/[strana-<vp-page>]]', array(
			'presenter' => "Changelog",
			'action' => array(
				Route::VALUE => 'archive'
			)
		));
		$router[] = new Route('/postava[/<id>-<name>]', array(
			'presenter' => "Character",
			'action' => array(
				Route::VALUE => 'view'
			)
		));
		$router[] = new Route('/stranka[/<id>-<webalized>]', array(
			'presenter' => "Pages",
			'action' => array(
				Route::VALUE => 'view'
			)
		));

		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

		return $router;
	}

}
