<?php


namespace App\AdminModule\presenters;


use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use App;

class PagesPresenter extends BasePresenter
{

	/** @var  App\Auth\PagesRepository @autowire */
	public $pagesRepository;


	private $selectedPage;



	public function startup()
	{
		parent::startup();

		$this->submenu->addSection("Stránky");
		$this->submenu->addItem("Pages:default", "Výpis stránek");
		$this->submenu->addItem("Pages:add", "Přidat stránku");
	}



	public function renderDefault()
	{
		$this->template->pages = $this->pagesRepository->getPages();
	}



	public function actionEdit($id)
	{
		$this->selectedPage = $this->pagesRepository->getPage($id);
	}



	public function createComponentAddPageForm()
	{
		$form = new Form();

		$form->addText("title", "Titulek")
			 ->setAttribute("class", "form-control");
		$form->addTextArea("text", "Text")
			 ->setAttribute("id", "ckEditor");

		$form->addSubmit("send", "Přidat stránku");
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->addPageFormSucceeded;

		return $form;
	}



	public function addPageFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->pagesRepository->addPage($values->title, $values->text, $this->user->id);
		$this->flashMessage("Stránka byla přidána", "success");
		$this->redirect("Pages:default");
	}



	public function createComponentEditPageForm()
	{
		$form = new Form();

		$form->addText("title", "Titulek")
			 ->setAttribute("class", "form-control");
		$form->addTextArea("text", "Text")
			 ->setAttribute("id", "ckEditor");

		$form->addSubmit("send", "Upravit stránku");

		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->editPageFormSucceeded;
		$form->setDefaults($this->selectedPage);

		return $form;
	}



	public function editPageFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->pagesRepository->editPage($this->selectedPage->id, $values->title, $values->text, $this->user->id);
		$this->flashMessage("Stránka byla upravena", "success");
		$this->redirect("Pages:default");
	}



	public function handleDeletePage($id)
	{
		$this->pagesRepository->deletePage($id);
		$this->flashMessage("Stránka byla smazána", "error");
		$this->redirect("Pages:default");
	}
} 