<?php


namespace App\AdminModule\presenters;

use App;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Brabijan;

class ArticlesPresenter extends BasePresenter
{

	/** @var  App\Web\ArticlesRepository @autowire */
	public $ArticleRepository;

	/** @var  integer */
	private $selectedArticle;

	use Brabijan\Images\TImagePipe;



	public function startup()
	{
		parent::startup();

		$this->submenu->addSection("Články");
		$this->submenu->addItem("Articles:default", "Výpis článků");
		$this->submenu->addItem("Articles:add", "Přidat článek");
	}



	public function renderDefault()
	{
		$this->template->articles = $this->ArticleRepository->getArticles();
	}



	public function actionEdit($id)
	{
		$this->selectedArticle = $this->ArticleRepository->getArticle($id);
	}



	public function createComponentAddArticleForm()
	{
		$form = new Form();

		$form->addUpload("thumbnail", "Nahrajte obrázek")
			 ->addRule(Form::FILLED, "Vyberte obrázek");
		$form->addText("title", "Titulek")
			 ->addRule(Form::FILLED, "Titulek musí být vyplněn")
			 ->setAttribute("class", "form-control");
		$form->addTextArea("text", "Text")
			 ->setAttribute("style", "width: 647px;height: 290px;")
			 ->setAttribute("class", "form-control")
			 ->addRule(Form::FILLED, "Text musí být vyplněn");

		$form->addSubmit("send", "Přidat článek");
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->addArticleFormSucceeded;

		return $form;
	}



	public function addArticleFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->ArticleRepository->addArticle($this->user->id, $values->thumbnail, $values->title, $values->text);
		$this->flashMessage("Článek byl přidán", "success");
		$this->redirect(":Admin:Articles:default");
	}



	public function createComponentEditArticleForm()
	{
		$form = new Form();

		$form->addUpload("thumbnail", "Nahrajte obrázek");
		$form->addText("title", "Titulek")
			 ->addRule(Form::FILLED, "Titulek musí být vyplněn");
		$form->addTextArea("text", "Text")
			 ->setAttribute("style", "width: 647px;height: 290px;")
			 ->addRule(Form::FILLED, "Text musí být vyplněn");

		$form->addSubmit("send", "Přidat článek");
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->setDefaults($this->selectedArticle);
		$form->onSuccess[] = $this->editArticleFormSucceeded;

		return $form;
	}



	public function editArticleFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->ArticleRepository->editArticle($this->selectedArticle->id, $this->user->id, $values->thumbnail, $values->title, $values->text);
		$this->flashMessage("Článek byl upraven", "success");
		$this->redirect(":Admin:Articles:default");
	}



	public function handleDeleteArticle($id)
	{
		$this->ArticleRepository->deleteArticle($id);
		$this->flashMessage("Článek byl smazán", "error");
		$this->redirect(":Admin:Articles:default");
	}

} 