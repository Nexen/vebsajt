<?php


namespace App\AdminModule\presenters;


use App;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Brabijan;

class SliderPresenter extends BasePresenter
{

	/** @var  App\Auth\SliderRepository @autowire */
	public $SliderRepository;

	/** @var  integer */
	private $selectedSlide;

	use Brabijan\Images\TImagePipe;



	public function startup()
	{
		parent::startup();

		$this->submenu->addSection("Slider");
		$this->submenu->addItem("Slider:default", "Výpis slidů");
		$this->submenu->addItem("Slider:add", "Přidat slide");
	}



	public function renderDefault()
	{
		$this->template->slides = $this->SliderRepository->getSlides()->order("order ASC");;
	}



	public function actionEdit($id)
	{
		$this->selectedSlide = $this->SliderRepository->getSlide($id);
	}



	public function createComponentAddSlideForm()
	{
		$form = new Form();

		$form->addUpload("slide", "Nahrajte slide")
			 ->addRule(Form::FILLED, "Vyberte slide");

		$form->addSubmit("send", "Přidat slide");
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());
		$form->onSuccess[] = $this->addSlideFormSucceeded;

		return $form;
	}



	public function addSlideFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->SliderRepository->addSlide($values->slide);
		$this->flashMessage("Slide byl přidán", "success");
		$this->redirect(":Admin:Slider:default");
	}



	public function handleDeleteSlide($id)
	{
		$this->SliderRepository->deleteSlide($id);
		$this->flashMessage("Slide byl smazán", "error");
		$this->redirect(":Admin:Slider:default");
	}



	public function handleMoveUp($id)
	{
		$this->SliderRepository->moveUp($id);
		$this->flashMessage("Slide byl posunut nahoru", "success");
		$this->redirect("this");
	}



	public function handleMoveDown($id)
	{
		$this->SliderRepository->moveDown($id);
		$this->flashMessage("Slide byl posunut dolu", "success");
		$this->redirect("this");
	}
} 