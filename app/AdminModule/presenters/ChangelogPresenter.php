<?php


namespace App\AdminModule\presenters;

use App;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette;

class ChangelogPresenter extends BasePresenter
{

	/** @var  App\Auth\ChangelogRepository @autowire */
	public $changelogRepository;

	/** @var  Nette\Database\Table\ActiveRow */
	private $selectedRecord;



	public function startup()
	{
		parent::startup();

		$this->submenu->addSection("Changelog");
		$this->submenu->addItem("Changelog:default", "Výpis changelogu");
		$this->submenu->addItem("Changelog:add", "Přidat záznam");
	}



	public function renderDefault()
	{
		$this->template->changelogReported = $this->changelogRepository->getChangelog()->where(array("status" => "Reported"));
		$this->template->changelogVerified = $this->changelogRepository->getChangelog()->where(array("status" => "Verified"));
		$this->template->changelogResolving = $this->changelogRepository->getChangelog()->where(array("status" => "Resolving"));
		$this->template->changelogFixed = $this->changelogRepository->getChangelog()->where(array("status" => "Fixed"));
		$this->template->changelogWeb = $this->changelogRepository->getChangelog()->where(array("status" => "Web"));
	}



	public function actionEdit($id)
	{
		$this->selectedRecord = $this->changelogRepository->getRecord($id);
	}



	public function createComponentAddRecordForm()
	{
		$form = new Form();
		$milestones = array(
			"Spell" => "Spell",
			"Quest" => "Quest",
			"NPC" => "NPC",
			"Glitch" => "Glitch",
			"Character" => "Character",
			"Web" => "Web"
		);
		$status = array(
			"Reported" => "Reported",
			"Verified" => "Verified",
			"Resolving" => "Resolving",
			"Fixed" => "Fixed",
			"Feature" => "Feature"
		);
		$form->addSelect("milestone", "Sekce:", $milestones);
		$form->addText("title", "Title: ")
			 ->setAttribute("class", "form-control");
		$form->addText("video", "Video: ")
			 ->setAttribute("class", "form-control");
		$form->addText("image", "Obrázek: ")
			 ->setAttribute("class", "form-control");
		$form->addSelect("status", "Status: ", $status);

		$form->addTextArea("description", "Popis")
			 ->setAttribute("id", "ckEditor");
		$form->addSubmit("send", "Odeslat");
		$form->onSuccess[] = $this->addRecordFormSucceeded;
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());

		return $form;
	}



	public function addRecordFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->changelogRepository->addRecord($values->milestone, $values->title, $values->description, $values->image, $values->video, $this->user->id, $values->status);
		$this->flashMessage("Záznam byl úspěšně přidán", "success");
		$this->redirect("this");
	}



	public function createComponentEditRecordForm()
	{
		$form = new Form();
		$milestones = array(
			"Spell" => "Spell",
			"Quest" => "Quest",
			"NPC" => "NPC",
			"Glitch" => "Glitch",
			"Character" => "Character",
			"Web" => "Web"
		);
		$status = array(
			"Reported" => "Reported",
			"Verified" => "Verified",
			"Resolving" => "Resolving",
			"Fixed" => "Fixed"
		);
		$form->addSelect("milestone", "Sekce:", $milestones);
		$form->addText("title", "Title: ");
		$form->addTextArea("description", "Popis")
			 ->setAttribute("id", "ckEditor");
		$form->addText("video", "Video: ");
		$form->addText("image", "Obrázek: ");
		$form->addSelect("status", "Status: ", $status);

		$form->addSubmit("send", "Odeslat");
		$form->onSuccess[] = $this->editRecordFormSucceeded;
		$form->setDefaults($this->selectedRecord);
		$form->setRenderer(new \Nextras\Forms\Rendering\Bs3FormRenderer());

		return $form;
	}



	public function editRecordFormSucceeded(Form $form)
	{
		$values = $form->getValues();

		$this->changelogRepository->updateRecord($this->selectedRecord->id, $values->milestone, $values->title, $values->description, $values->image, $values->video, $this->user->id, $values->status);
		$this->flashMessage("Záznam byl úspěšně upraven", "success");
		$this->redirect("Changelog:default");
	}
} 