<?php

namespace App\AdminModule\presenters;

use App;
use Brabijan;

class BasePresenter extends \App\Presenters\SecuredPresenter
{

	/**
	 * @var Brabijan\BootstrapUIComponents\Submenu\Control $submenu
	 */
	protected $submenu;



	public function __construct()
	{
		$factory = new Brabijan\BootstrapUIComponents\Submenu\Factory;
		$this->submenu = $factory->create();
	}



	public function createComponentSubmenu()
	{
		return $this->submenu;
	}

} 